package com.ProVote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvoteApplication.class, args);
	}
}
