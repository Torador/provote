package com.ProVote.provote.services;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ProVote.provote.entities.Student;
import com.ProVote.provote.metiers.StudentMetier;
import com.ProVote.provote.repositories.StudentRepository;

@Service
public class StudentService implements StudentMetier {

	private Log log;
	
	@Autowired
	private StudentRepository studentRepository;

	@Override
	public Student save(Student student) {
		student.setCreateAt(new Date());
		return studentRepository.save(student);
	}

	@Override
	public List<Student> ListStudent() {

		return studentRepository.findAll();
	}

	@Override
	public Student update(Student student, Long id) {
		try {
			Student st = studentRepository.findById(id).orElse(null);
			if(st == null) {
				return null;
			}
			student.setId(st.getId());
			return studentRepository.save(student);
		} catch (Exception e) {
			log.error("Line59: ERROR OCCURED =>"+ e.getMessage(), e);
			return null;
		}
		
	}

	@Override
	public boolean deleteById(Long id) {
		
		studentRepository.deleteById(id);

		return true;
	}

	

}
