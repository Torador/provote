package com.ProVote.provote.services;

import java.util.Date;
import java.util.List;

import com.ProVote.provote.entities.Project;
import com.ProVote.provote.metiers.ProjectMetier;
import com.ProVote.provote.repositories.ProjectRepository;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService implements ProjectMetier {

  private Log log;

  @Autowired
  private ProjectRepository projectRepository;

  @Override
  public Project save(Project p) {
    Project project = new Project();
    project.setCreator(p.getCreator());
    project.setMembers(p.getMembers());
    project.setLabel(p.getLabel());
    project.setDescription(p.getDescription());
    project.setSlug(new Date().toString());
    project.setUrlDemo(p.getUrlDemo());
    project.setThumb(p.getThumb());
    project.setCreateAt(new Date());

    return projectRepository.save(project);

  }

  @Override
  public List<Project> getProjects() {
    return projectRepository.findAll();
  }

  @Override
  public Project update(Project project, Long id) {
    try {
      Project p = projectRepository.findById(id).orElse(null);
      if (p == null) {
        return null;
      }
      project.setId(p.getId());
      return projectRepository.save(project);
    } catch (Exception e) {
      log.error("Line48: ERROR OCCURED => " + e.getMessage(), e);
      return null;
    }
  }

  @Override
  public Boolean delete(Long id) {
    try {
      projectRepository.deleteById(id);
      return true;
    } catch (Exception e) {
      log.error("Line59: ERROR OCCURED => " + e.getMessage(), e);
      return false;
    }
  }
}
