package com.ProVote.provote.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ProVote.provote.repositories.ProjectRepository;
import com.ProVote.provote.entities.Project;
import com.ProVote.provote.entities.Student;
import com.ProVote.provote.entities.Vote;
import com.ProVote.provote.metiers.VoteMetier;
import com.ProVote.provote.repositories.StudentRepository;
import com.ProVote.provote.repositories.VoteRepository;

@Service
public class VoteService implements VoteMetier {

	@Autowired
	private VoteRepository voteRepository;

	@Autowired

	@Override
	public List<Vote> showAll() {
		return voteRepository.findAll();
	}

	@Override
	public List<Vote> showVoteByStudent(Student student) {
		return voteRepository.findByStudent(student);
	}

	@Override
	public boolean goVote(Student student, Project project) {

		try {

			if (project == null)
				return false;

			if (student == null)
				return false;

			List<Vote> v = voteRepository.findByProjectIdAndStudentId(project, student);
			if (v.size() > 0) {
				return false;
			}

			Vote vote = new Vote();
			vote.setProject(project);
			vote.setStudent(student);
			vote.setCreateAt(new Date());
			System.out.println("DONE...");
			voteRepository.save(vote);
			return true;

		} catch (Exception e) {

			System.out.println("Line71: ERROR OCCURED => " + e);
			return false;

		}

	}

	@Override
	public boolean cancelVote(Long idVote) {

		try {

			Vote vote = voteRepository.findById(idVote).orElse(null);
			if (vote == null) {
				return false;
			}

			voteRepository.delete(vote);
			return true;
		} catch (Exception e) {
			System.out.println("Line87: ERROR OCCURED => " + e);
			return false;
		}

	}

}
