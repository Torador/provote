package com.ProVote.provote.repositories;

import com.ProVote.provote.entities.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

  @Query("SELECT u FROM Users u WHERE u.username = ?1")
  Users findByUsername(String username);

}
