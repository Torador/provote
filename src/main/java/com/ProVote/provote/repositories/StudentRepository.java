package com.ProVote.provote.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import com.ProVote.provote.entities.Student;
import com.ProVote.provote.entities.Users;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

  @Query("SELECT s FROM Student s WHERE s.user = ?1")
  public Optional<Student> findByUser(Users user);

}
