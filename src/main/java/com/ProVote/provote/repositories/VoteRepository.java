package com.ProVote.provote.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ProVote.provote.entities.Project;
import com.ProVote.provote.entities.Student;
import com.ProVote.provote.entities.Vote;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {

	@Query("SELECT v FROM Vote v WHERE v.project =?1 AND v.student = ?2")
	public List<Vote> findByProjectIdAndStudentId(Project project, Student student);

	@Query("SELECT v FROM Vote v WHERE v.student = ?1")
	public List<Vote> findByStudent(Student student);

	@Query("SELECT v FROM Vote v WHERE v.student = ?1")
	public List<Vote> findStudentWhoDontVoted(Student student);

}
