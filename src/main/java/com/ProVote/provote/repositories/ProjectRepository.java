package com.ProVote.provote.repositories;

import com.ProVote.provote.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {
}
