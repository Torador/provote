package com.ProVote.provote.controllers;

import java.util.Date;
import java.util.List;

import com.ProVote.provote.config.JwtTokenUtil;
import com.ProVote.provote.entities.Users;
import com.ProVote.provote.repositories.UserRepository;
import com.ProVote.provote.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

  @Autowired
  UserRepository userRepository;

  @Autowired
  UserService userDetailsService;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @GetMapping
  public ResponseEntity<List<Users>> showAll() {
    return ResponseEntity.ok(userDetailsService.getUsers());
  }

  @PostMapping("/signup")
  public ResponseEntity<?> registerUser(@RequestBody Users user) {
    Users us = userRepository.findByUsername(user.getUsername());
    // System.out.println("CURRENT USER: " + us);
    if (us == null) {
      user.setCreatedAt(new Date());

      user.setPassword(user.getPassword());

      return ResponseEntity.ok(userRepository.save(user));
    }

    return ResponseEntity.badRequest().body("This user is already exist.");
  }

  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@RequestBody Users user) throws Exception {
    Users u = userRepository.findByUsername(user.getUsername());
    System.out.println("CURRENT USER: " + u);

    if (u != null) {

      if (u.getPassword().equals(user.getPassword())) {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(token);

      }

      return ResponseEntity.badRequest().body("Bad credentials.");
    }

    return ResponseEntity.badRequest().body("You are not registered yet.");

  }

}
