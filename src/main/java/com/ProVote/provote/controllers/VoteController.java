package com.ProVote.provote.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.ProVote.provote.config.JwtTokenUtil;
import com.ProVote.provote.entities.Project;
import com.ProVote.provote.entities.Student;
import com.ProVote.provote.entities.Users;
import com.ProVote.provote.entities.Vote;
import com.ProVote.provote.repositories.ProjectRepository;
import com.ProVote.provote.repositories.StudentRepository;
import com.ProVote.provote.repositories.UserRepository;
import com.ProVote.provote.services.VoteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/vote")
public class VoteController {

	@Autowired
	private VoteService voteService;

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private ProjectRepository projectRepo;

	@Autowired
	private StudentRepository studentRepo;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@GetMapping("/")
	public List<Vote> getVotes(HttpServletRequest request) {

		return voteService.showAll();
	}

	@GetMapping("/student")
	public ResponseEntity<?> getVotesByStudent(@RequestHeader("Authorization") String fullToken) {
		try {
			Users usr = getInfoInToken(fullToken);
			Student std = studentRepo.findByUser(usr).orElse(null);
			return ResponseEntity.ok(voteService.showVoteByStudent(std));
		} catch (Exception e) {
			System.out.println("ERREUR(line 49): " + e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("OUPS, NOUS AVONS RENCONTRÉ UN PROBLÈME: " + e.getMessage());
		}
	}

	@PostMapping("/govote")
	public ResponseEntity<?> goVote(@RequestHeader("Authorization") String fullToken, @RequestBody Project project) {

		try {
			System.out.println("PROJECT ID: " + project.getId());
			Users usr = getInfoInToken(fullToken);
			System.out.println("User: " + usr.getId());
			Student std = studentRepo.findByUser(usr).orElse(null);

			if (std == null) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("This student can't voted. Please complete account info profil to process.");
			}

			System.out.println("CONTENT: " + std.getId() + " " + std.getFirstname());

			Project prj = projectRepo.findById(project.getId()).orElse(null);
			if (prj == null) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("This project is not exist. Please try on another project.");
			}

			System.out.println("CONTENT: " + prj.getId() + " " + prj.getLabel());

			return ResponseEntity.ok(voteService.goVote(std, prj));
		} catch (Exception e) {
			System.out.println("ERREUR(line 69): " + e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("OUPS, PROBLEM OCCURED: " + e.getMessage());
		}
	}

	@DeleteMapping("/cancel/{idVote}")
	public boolean cancelVote(@PathVariable Long idVote) {

		return voteService.cancelVote(idVote);
	}

	private Users getInfoInToken(String fullToken) {
		String token = fullToken.substring(7);
		String usernameOfUser = jwtTokenUtil.getUsernameFromToken(token);
		System.out.println(" USERNAMETOKEN: " + usernameOfUser);
		return userRepo.findByUsername(usernameOfUser);
	}
}
