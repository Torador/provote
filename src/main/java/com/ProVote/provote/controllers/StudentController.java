package com.ProVote.provote.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ProVote.provote.entities.Student;
import com.ProVote.provote.metiers.StudentMetier;

@RestController
@RequestMapping("/api/v1/student")
public class StudentController {

	@Autowired
	private StudentMetier studentMetier;

	@PostMapping("/save")
	public Student save(@RequestBody Student student) {
		return studentMetier.save(student);
	}

	@GetMapping("/")
	public List<Student> ListStudent() {
		return studentMetier.ListStudent();
	}

	@PutMapping("/update/{id}")
	public Student update(@RequestBody Student student, @PathVariable Long id) {
		return studentMetier.update(student, id);
	}

	@DeleteMapping("/delete/{id}")
	public boolean deleteById(@PathVariable Long id) {
		return studentMetier.deleteById(id);
	}

}
