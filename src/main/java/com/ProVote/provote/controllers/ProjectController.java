package com.ProVote.provote.controllers;

import java.util.List;

import com.ProVote.provote.entities.Project;
import com.ProVote.provote.services.ProjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/project")
public class ProjectController {

  @Autowired
  private ProjectService projectService;

  @GetMapping("/")
  public List<Project> get() {
    return projectService.getProjects();
  }

  @PostMapping("/save")
  public Project post(@RequestBody Project p) {
    return projectService.save(p);
  }

  @PutMapping("/update/{id}")
  public Project put(@RequestBody Project p, @PathVariable Long id) {
    return projectService.update(p, id);
  }

  @DeleteMapping("/delete/{id}")
  public Boolean delete(@PathVariable Long id) {
    return projectService.delete(id);
  }

}
