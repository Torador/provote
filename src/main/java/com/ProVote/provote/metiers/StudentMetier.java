package com.ProVote.provote.metiers;

import java.util.List;

import com.ProVote.provote.entities.Student;

public interface StudentMetier {
	
	public Student save(Student student);
	public List<Student> ListStudent();
	public Student update(Student student, Long id);
	public boolean deleteById(Long id);

}
