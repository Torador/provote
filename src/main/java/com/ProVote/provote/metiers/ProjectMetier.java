package com.ProVote.provote.metiers;

import java.util.List;

import com.ProVote.provote.entities.Project;

public interface ProjectMetier {
  public Project save(Project project);

  public List<Project> getProjects();

  public Project update(Project project, Long id);

  public Boolean delete(Long id);
}
