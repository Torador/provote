package com.ProVote.provote.metiers;

import java.util.List;

import com.ProVote.provote.entities.Project;
import com.ProVote.provote.entities.Student;
import com.ProVote.provote.entities.Vote;

public interface VoteMetier {

	public List<Vote> showAll();

	public List<Vote> showVoteByStudent(Student student);

	public boolean goVote(Student student, Project project);

	public boolean cancelVote(Long idVote);

}
